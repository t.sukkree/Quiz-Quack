 package com.quizquack.game;

import java.util.ArrayList;
import java.util.HashMap;

public class GenKnowledge extends Question{

	private ArrayList<HashMap<String,String>> questionSet;
	
	public GenKnowledge() {
		genQuestion();
		randomQuestion();
		randomChoice();
	}
	
	@Override
	public ArrayList getQuestionSet() {
		return questionSet;
	}
	
	public ArrayList<HashMap<String, String>> genQuestion() {
		questionSet = new ArrayList<HashMap<String,String>>();
		HashMap<String,String> q = new HashMap<String,String>();
		
		q.put("question", "วัดพระธาตุลำปางหลวงอยู่ที่อำเภออะไร");
		q.put("c1", "บางเขน");
		q.put("c2", "เชียงใหม่");
		q.put("c3", "อ.เมือง");
		q.put("c4", "เกาะคา");
		q.put("answer", "c4");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "แม่น้ำป่าสักมีต้นกำเนิดจากจังหวัดใด");
		q.put("c1", "เลย");
		q.put("c2", "ลพบุรี");
		q.put("c3", "สระบุรี");
		q.put("c4", "อยุธยา");
		q.put("answer", "c1");
		questionSet.add(q);
	//	System.out.println(questionSet.get(0));
		
		q = new HashMap<String,String>();
		q.put("question", "แม่น้ำตาปี มีอีกชื่อหนึ่งว่าอะไร");
		q.put("c1", "แม่น้ำพุมดวง");
		q.put("c2", "แม่น้ำหลวง");
		q.put("c3", "แม่น้ำเจ้าพระยา");
		q.put("c4", "แม่น้ำเขาราช");
		q.put("answer", "c2");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "อุทยานแห่งชาติเขาพนมเบญจา อยู่จังหวัดใด");
		q.put("c1", "เชียงใหม่");
		q.put("c2", "ลำพูน");
		q.put("c3", "กาญจนบุรี");
		q.put("c4", "กระบี่");
		q.put("answer", "c4");
		questionSet.add(q);
		
		q = new HashMap<String,String>();
		q.put("question", "วันเด็กแห่งชาติ ตรงกับวันใด");
		q.put("c1", "วันแรกของปี");
		q.put("c2", "วันศุกร์แรกของปี");
		q.put("c3", "วันเสาร์ที่ 2 ของเดือนมกราคม");
		q.put("c4", "วันอาทิตย์ที่ 2 ของเดือนมกราคม");
		q.put("answer", "c3");
		questionSet.add(q);
		
		q = new HashMap<String,String>();
		q.put("question", "สะพานที่ยาวที่สุดของไทย ชื่ออะไร");
		q.put("c1", "สะพานรักสารสิน");
		q.put("c2", "สะพานรัฐชดา");
		q.put("c3", "สะพานติณสูลานนท์");
		q.put("c4", "สะพานพุธ");
		q.put("answer", "c3");
		questionSet.add(q);
		
		q = new HashMap<String,String>();
		q.put("question", "นักกีฬาไทยคนแรก ที่ได้เหรียญจากกีฬาโอลิมปิค คือใคร");
		q.put("c1", "พะเยาว์ พูนธรัตน์");
		q.put("c2", "สามารถ พยัคอรุณ");
		q.put("c3", "วิเชพ ใจบุญ");
		q.put("c4", "สมรัก คำสิงห์");
		q.put("answer", "c1");
		questionSet.add(q);
		
		q = new HashMap<String,String>();
		q.put("question", "จังหวัดที่มีเนื้อที่น้อยที่สุด คือจังหวัดใด");
		q.put("c1", "ภูเก็ต");
		q.put("c2", "สมุทรสาคร");
		q.put("c3", "ยะลา");
		q.put("c4", "ตรัง");
		q.put("answer", "c2");
		questionSet.add(q);
		
		q = new HashMap<String,String>();
		q.put("question", "พระธาตุดอยกองมู อยู่จังหวัดใด");
		q.put("c1", "พะเยา");
		q.put("c2", "ตาก");
		q.put("c3", "ตรัง");
		q.put("c4", "แม่ฮ่องสอน");
		q.put("answer", "c4");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "เมืองเกาะครึ่งร้อย คือจังหวัดใด");
		q.put("c1", "ตราด");
		q.put("c2", "กระบี่");
		q.put("c3", "ภูเก็ต");
		q.put("c4", "ระนอง");
		q.put("answer", "c1");
		questionSet.add(q);
		
		q = new HashMap<String,String>();
		q.put("question", "เทือกเขาพรมดงรัก กั้น 3 ประเทศคือ ประเทศใดบ้าง");
		q.put("c1", "ไทย ลาว พม่า");
		q.put("c2", "ไทย กัมพูชา จีน");
		q.put("c3", "ไทย กัมพูชา ลาว");
		q.put("c4", "ไทย จีน ลาว");
		q.put("answer", "c3");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "เขื่อนศรีนครินทร์ มีชื่ออีกชื่อหนึ่งว่าอะไร");
		q.put("c1", "เขื่อนยันฮี");
		q.put("c2", "เชื่อนผาซ่อม");
		q.put("c3", "เขื่อนเจ้าเณร");
		q.put("c4", "เขื่อนลำโดมน้อย");
		q.put("answer", "c3");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "ผู้แต่งเพลงสรรเสริญบารมี คือใคร");
		q.put("c1", "หลวงสารานุประพันธ์");
		q.put("c2", "สมเด็จพระเจ้าบรมวงศ์เธอ เจ้าฟ้ากรมพระยานริศรานุวัติวงศ์");
		q.put("c3", "กรมหลวงชุมพรเขตอุดมศักดิ์");
		q.put("c4", "กรมหลวงราชบุรีดิเรกฤทธิ์");
		q.put("answer", "c2");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "พระราชวังสนามจันทร์ อยู่ที่จังหวัดใด");
		q.put("c1", "สมุทรสาคร");
		q.put("c2", "สมุทรสงคราม");
		q.put("c3", "สมุทรปราการ");
		q.put("c4", "นครปฐม");
		q.put("answer", "c4");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "พระบาทสมเด็จพระพุทธยอดฟ้าจุฬาโลก คือรัชกาลที่เท่าใด");
		q.put("c1", "1");
		q.put("c2", "3");
		q.put("c3", "7");
		q.put("c4", "8");
		q.put("answer", "c1");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "พรมแดนกั้นไทยกับประเทศลาว สิ่งต่อไปนี้กั้นอยู่");
		q.put("c1", "ภูเขาแดนเมือง");
		q.put("c2", "ภูเขาพนมดงรัก");
		q.put("c3", "แม่น้ำท่าจีน");
		q.put("c4", "แม่น้ำเมย");
		q.put("answer", "c1");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "แม่น้ำปิง มีต้นกำเนิดจากจังหวัดใด");
		q.put("c1", "เชียงใหม่");
		q.put("c2", "ลำปาง");
		q.put("c3", "เชียงราย");
		q.put("c4", "น่าน");
		q.put("answer", "c1");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "อุทยานแห่งชาติภูกระดึง อยู่จังหวัดใด");
		q.put("c1", "ลำปาง");
		q.put("c2", "เลย");
		q.put("c3", "สกลนคร");
		q.put("c4", "พิษณุโลก");
		q.put("answer", "c2");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "อุทยานแห่งชาติคลองลาน อยู่จังหวัดใด");
		q.put("c1", "กำแพงเพชร");
		q.put("c2", "อุบลราชธานี");
		q.put("c3", "สุราษฏร์ธานี");
		q.put("c4", "พัทลุง");
		q.put("answer", "c1");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "ชื่อเดิมของประเทศไทยคือ อะไร");
		q.put("c1", "สยาม");
		q.put("c2", "สุโขทัย");
		q.put("c3", "อยุธยา");
		q.put("c4", "กรุงเทพฯ");
		q.put("answer", "c1");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "หนองน้ำที่ใหญ่ที่สุดของไทย คืออะไร");
		q.put("c1", "หนองบวกหาด เชียงใหม่");
		q.put("c2", "หนองตึงเฒ่า เชียงใหม่");
		q.put("c3", "หนองหาน สกลนคร");
		q.put("c4", "หนองบรเพชร เพชรบุรี");
		q.put("answer", "c3");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "นักสนุ้กเกอร์ไทยคนแรก ที่ติดอันดับโลก คือใคร");
		q.put("c1", "เขาทราย กาแลกซี่");
		q.put("c2", "วัฒนา ภู่โอบอ้อม");
		q.put("c3", "ลักษณะ รุจจนพันธุ์");
		q.put("c4", "Tiger wood");
		q.put("answer", "c2");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "จังหวัดที่อยู่เหนือสุดคือจังหวัดใด");
		q.put("c1", "เชียงราย");
		q.put("c2", "พะเยา");
		q.put("c3", "เชียงใหม่");
		q.put("c4", "แม่ฮ่องสอน");
		q.put("answer", "c1");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "จังหวัดใดมี ฝนแปด แดดสี่");
		q.put("c1", "ระนอง");
		q.put("c2", "ระยอง");
		q.put("c3", "แม่ฮ่องสอน");
		q.put("c4", "กระบี่");
		q.put("answer", "c1");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "งานประเพณี อุ้มประดำน้ำ มีที่จังหวัดใด");
		q.put("c1", "ตาก");
		q.put("c2", "นครสวรรค์");
		q.put("c3", "เพชรบูรณ์");
		q.put("c4", "นครราชสีมา");
		q.put("answer", "c3");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "แม่น้ำที่ยาวที่สุดในภาคตะวันออกเฉียงเหนือ คือแม่น้ำใด");
		q.put("c1", "แม่น้ำชี");
		q.put("c2", "แม่น้ำยม");
		q.put("c3", "แม่น้ำโกลก");
		q.put("c4", "แม่น้ำโขง");
		q.put("answer", "c1");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "เขื่อนสิริกิติ์ อยู่จังหวัดใด");
		q.put("c1", "ตาก");
		q.put("c2", "อุตรดิตถ์");
		q.put("c3", "ศรีสะเกษ");
		q.put("c4", "ระยอง");
		q.put("answer", "c2");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "วัดที่อยู่ในประบรมมหาราชวัง คือวัดอะไร");
		q.put("c1", "วัดแจ้ง");
		q.put("c2", "วัดพระแก้ว");
		q.put("c3", "วัดพระเชตุพนวิมลมังคลาราม");
		q.put("c4", "วัดอรุณราชวราราม");
		q.put("answer", "c2");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "คำขวัญ ส้มโอหวาน ข้าวสารขาว ลูกสาวสวย เป็นคำขวัญของจังหวัดใด");
		q.put("c1", "นครราชสีมา");
		q.put("c2", "ลพบุรี");
		q.put("c3", "ราชบุรี");
		q.put("c4", "นครปฐม");
		q.put("answer", "c4");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "พระบาทสมเด็จพระมงกุฎเกล้าเจ้าอยู่หัว คือรัชกาลที่เท่าใด");
		q.put("c1", "3");
		q.put("c2", "4");
		q.put("c3", "5");
		q.put("c4", "6");
		q.put("answer", "c4");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "รัฐธรรมนูญแห่งราชอาณาจักรไทย 2550 มีกี่หมวด กี่มาตรา");
		q.put("c1", "16 หมวด 319 มาตรา");
		q.put("c2", "18 หมวด 329 มาตรา");
		q.put("c3", "12 หมวด 319 มาตรา");
		q.put("c4", "15 หมวด 309 มาตรา");
		q.put("answer", "c4");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "ประเทศที่เข้าเป็นสมาชิกอาเซียนล่าสุดคือประเทศใด");
		q.put("c1", "กัมพูชา");
		q.put("c2", "เวียดนาม");
		q.put("c3", "บรูไน");
		q.put("c4", "ลาว");
		q.put("answer", "c1");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "อาเซียนก่อตั้งครั้งแรกมีกี่ประเทศ ประเทศอะไรบ้าง");
		q.put("c1", "3 ประเทศ คือ ไทย สิงคโปร์ มาเลเซีย");
		q.put("c2", "4 ประเทศ คือ ไทย สิงคโปร์ มาเลเซีย อินโดนีเซีย");
		q.put("c3", "4 ประเทศ คือ ไทย สิงคโปร์ มาเลเซีย ฟิลิปปินส์");
		q.put("c4", "5 ประเทศ ได้แก่ ไทย สิงคโปร์ มาเลเซีย ฟิลิปปินส์ อินโดนีเซีย");
		q.put("answer", "c4");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "บุคคลในข้อใดมีสามารถไปใช้สิทธิเลือกตั้งได้");
		q.put("c1", "นักพรต นักบวช");
		q.put("c2", "คนที่อยู่ในระหว่างต้องคุมขังโดยหมายของศาล");
		q.put("c3", "ผู้ที่ไปทำงานหรืออาศัยอยู่นอกประเทศ");
		q.put("c4", "วิกลจริต หรือจิตฟั่นเฟือนไม่สมประกอบ");
		q.put("answer", "c3");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "ตามรัฐธรรมนูญแห่งราชอาณาจักรไทย 2550 บุคคลมีสิทธิเสมอกันในการรับการศึกษาไม่น้อยกว่ากี่ปี");
		q.put("c1", "15 ปี");
		q.put("c2", "12 ปี");
		q.put("c3", "10 ปี");
		q.put("c4", "8 ปี");
		q.put("answer", "c2");
		questionSet.add(q);

		/*q = new HashMap<String,String>();
		q.put("question", "ศาลใดมีหน้าที่พิจารณาและวินิจฉัยความขัดแย้งเกี่ยวกับอำนาจหน้าที่ระหว่างรัฐสภา คณะรัฐมนตรี หรือองค์กรตามรัฐธรรมนูญที่มิใช่ศาล");
		q.put("c1", "ศาลยุติธรรม");
		q.put("c2", "ศาลรัฐธรรมนูญ");
		q.put("c3", "ศาลปกครอง");
		q.put("c4", "ศาลฎีกา");
		q.put("answer", "c2");
		questionSet.add(q);*/

		q = new HashMap<String,String>();
		q.put("question", "สีน้ำเงิน ในสัญลักษณ์ของอาเซียน หมายถึงข้อใด");
		q.put("c1", "ความเจริญรุ่งเรือง");
		q.put("c2", "ความกล้าหาญ");
		q.put("c3", "สันติภาพ");
		q.put("c4", "ความก้าวหน้า");
		q.put("answer", "c3");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "รัฐสภาประกอบด้วยอะไร");
		q.put("c1", "นายกรัฐมนตรี รัฐสภา");
		q.put("c2", "รัฐสภา คณะรัฐมนตรี");
		q.put("c3", "ประธานรัฐสภา สมาชิกสภาผู้แทนราษฎร");
		q.put("c4", "สมาชิกสภาผู้แทนราษฎร สมาชิกวุฒิสภา");
		q.put("answer", "c4");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "ตำแหน่งทางการเมืองที่ต้องมีการตรวจสอบทรัพย์สินทุกครั้งที่รับและพ้นตำแหน่ง ได้แก่ใคร");
		q.put("c1", "นายกรัฐมนตรี");
		q.put("c2", "สมาชิกสภาผู้แทนราษฎร และสมาชิกวุฒิสภา");
		q.put("c3", "ผู้บริหารและสมาชิกสภาท้องถิ่น");
		q.put("c4", "ถูกทุกข้อ");
		q.put("answer", "c4");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "ตามรัฐธรรมนูญแห่งราชอาณาจักรไทย 2550 พระมหากษัตริย์ ทรงใช้อำนาจทางใด");
		q.put("c1", "ทางรัฐสภา นายกรัฐมนตรี และทหาร");
		q.put("c2", "ทางนายกรัฐมนตรี รัฐสภา และศาล");
		q.put("c3", "ทางรัฐสภา คณะรัฐมนตรี และทหาร");
		q.put("c4", "ทางรัฐสภา คณะรัฐมนตรี และศาล");
		q.put("answer", "c4");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "คณะรัฐมนตรี มีจำนวนไม่เกินกี่คน");
		q.put("c1", "35 คน");
		q.put("c2", "32 คน");
		q.put("c3", "28 คน");
		q.put("c4", "25 คน");
		q.put("answer", "c1");
		questionSet.add(q);

		q = new HashMap<String,String>();
	/*	q.put("question", "องค์กรตามรัฐธรรมนูญ มีกี่ประเภท");
		q.put("c1", "1 ประเภท คือ องค์กรตามรัฐธรรมนูญ");
		q.put("c2", "2 ประเภท คือ องค์กรอิสระตามรัฐธรรมนูญ องค์กรอื่นตามรัฐธรรมนูญ");
		q.put("c3", "2 ประเภท คือ องค์กรตามรัฐธรรมนูญ องค์กรอิสระตามรัฐธรรมนูญ");
		q.put("c4", "3 ประเภท คือ องค์กรตามรัฐธรรมนูญ องค์กรอิสระตามรัฐธรรมนูญ องค์กรอื่นตามรัฐธรรมนูญ");
		q.put("answer", "c2");
		questionSet.add(q);
		q.clear();*/
		
		/*q.put("question", "องค์กรอื่นตามรัฐธรรมนูญ มีกี่องค์กร อะไรบ้าง");
		q.put("c1", "3 องค์กร คือ องค์กรอัยการ คณะกรรมการสิทธิมนุษยชนแห่งชาติ สภาที่ปรึกษาเศรษฐกิจและสังคมแห่งชาติ");
		q.put("c2", "3 องค์กร คือ ผู้ตรวจการแผ่นดิน คณะกรรมการสิทธิมนุษยชนแห่งชาติ คณะกรรมการตรวจเงินแผ่นดิน");
		q.put("c3", "4 องค์กร คือ คณะกรรมการการเลือกตั้ง คณะกรรมการป้องกันและปราบปรามการทุจริตแห่งชาติ องค์กรอัยการ ผู้ตรวจการแผ่นดิน");
		q.put("c4", "4 องค์กร คือ คณะกรรมการป้องกันและปราบปรามการทุจริตแห่งชาติ สภาที่ปรึกษาเศรษฐกิจและสังคมแห่งชาติ คณะกรรมการตรวจเงินแผ่นดิน คณะกรรมการสิทธิมนุษยชนแห่งชาติ");
		q.put("answer", "c1");
		questionSet.add(q);
		q.clear();*/
		
		q.put("question", "จำนวนสมาชิกสภาผู้แทนราษฎร (ส.ส.) กำหนดไว้กี่คน");
		q.put("c1", "400 คน");
		q.put("c2", "450 คน");
		q.put("c3", "500 คน");
		q.put("c4", "550 คน");
		q.put("answer", "c3");
		questionSet.add(q);

		q = new HashMap<String,String>();
		/*q.put("question", "ข้อใดไม่ใช่คุณสมบัติของบุคคลที่มีสิทธิเลือกตั้ง");
		q.put("c1", "มีอายุไม่ต่ำกว่า 18 ปีบริบูรณ์ ในวันที่ 1 มกราคม ของปีที่มีการเลือกตั้ง");
		q.put("c2", "มีรายชื่อในทะเบียนบ้านในเขตเลือกตั้งไม่น้อยกว่า 90 วัน นับถึงวันเลือกตั้ง");
		q.put("c3", "มีสัญชาติไทย");
		q.put("c4", "กรณีแปลงสัญชาติต้องได้รับสัญชาติไม่น้อยกว่า 2 ปี");
		q.put("answer", "c4");
		questionSet.add(q);
		q.clear();*/
		
		q.put("question", "รัฐธรรมนูญแห่งราชอาณาจักรไทย 2550 บังคับใช้เมื่อใด");
		q.put("c1", "วันที่ 24 สิงหาคม 2550 เป็นปีที่ 62 ในรัชกาลปัจจุบัน");
		q.put("c2", "วันที่ 20 สิงหาคม 2550 เป็นปีที่ 60 ในรัชกาลปัจจุบัน");
		q.put("c3", "วันที่ 24 มิถุนายน 2550 เป็นปีที่ 62 ในรัชกาลปัจจุบัน");
		q.put("c4", "วันที่ 26 สิงหาคม 2550 เป็นปีที่ 60 ในรัชกาลปัจจุบัน");
		q.put("answer", "c1");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "รัฐธรรมนูญแห่งราชอาณาจักรไทย 2550 มีแก้ไขเพิ่มเติมกี่ฉบับ");
		q.put("c1", "1 ฉบับ");
		q.put("c2", "2 ฉบับ");
		q.put("c3", "4 ฉบับ");
		q.put("c4", "6 ฉบับ");
		q.put("answer", "c2");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "ผู้ที่ดำรงตำแหน่งรองประธานรัฐสภา คือใคร");
		q.put("c1", "นายกรัฐมนตรี");
		q.put("c2", "ประธานสภาผู้แทนราษฎร");
		q.put("c3", "ประธานวุฒิสภา");
		q.put("c4", "ปลัดสำนักนายกรัฐมนตรี");
		q.put("answer", "c3");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "ศาลยุติธรรมมีกี่ระดับ อะไรบ้าง");
		q.put("c1", "2 ระดับ คือ ศาลชั้นต้น ศาลฎีกา");
		q.put("c2", "2 ระดับ คือ ศาลชั้นต้น ศาลอุทธรณ์");
		q.put("c3", "3 ระดับ คือ ศาลชั้นต้น ศาลฎีกา ศาลปกครอง");
		q.put("c4", "3 ระดับ คือ ศาลชั้นต้น ศาลอุทธรณ์ ศาลฎีกา");
		q.put("answer", "c4");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "สถานที่ตั้งสำนักงานเลขาธิการอาเซียน คือที่ใด");
		q.put("c1", "กรุงเทพมหานคร ประเทศไทย");
		q.put("c2", "กรุงจากาตาร์ ประเทศอินโดนีเซีย");
		q.put("c3", "กรุงมะนิลา ฟิลิปปินส์");
		q.put("c4", "กรุงกัวลาลัมเปอร์ มาเลเซีย");
		q.put("answer", "c2");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "ในหมวดของ ศาล ตามรัฐธรรมนูญแห่งราชอาณาจักรไทย 2550 ศาลมีกี่ประเภท");
		q.put("c1", "3 ประเภท คือ ศาลชั้นต้น ศาลอุทธรณ์ ศาลฎีกา");
		q.put("c2", "3 ประเภท คือ ศาลยุติธรรม ศาลปกครอง ศาลทหาร");
		q.put("c3", "4 ประเภท คือ ศาลชั้นต้น ศาลรัฐธรรมนูญ ศาลปกครอง ศาลฏีกา");
		q.put("c4", "4 ประเภทคือ ศาลรัฐธรรมนูญ ศาลยุติธรรม ศาลปกครอง ศาลทหาร");
		q.put("answer", "c4");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "จำนวนสมาชิกวุฒิสภา (ส.ว.) กำหนดไว้กี่คน และได้มาโดยวิธีใด");
		q.put("c1", "150 คนจากการเลือกตั้ง 77 คน, การสรรหา 73 คน");
		q.put("c2", "150 คนจากการเลือกตั้ง 73คน, การสรรหา 77 คน");
		q.put("c3", "250 คนจากการเลือกตั้ง 150คน, การสรรหา 100 คน");
		q.put("c4", "250 คนจากการเลือกตั้ง 100คน, การสรรหา 150 คน");
		q.put("answer", "c1");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "ผู้ที่ดำรงตำแหน่งประธานรัฐสภา คือใคร");
		q.put("c1", "นายกรัฐมนตรี");
		q.put("c2", "ประธานสภาผู้แทนราษฎร");
		q.put("c3", "ประธานวุฒิสภา");
		q.put("c4", "ปลัดสำนักนายกรัฐมนตรี");
		q.put("answer", "c2");
		questionSet.add(q);

		q = new HashMap<String,String>();
	/*	q.put("question", "ความหมายของสัญลักษณ์อาเซียน คือข้อใด");
		q.put("c1", "ประเทศสมาชิกรวมกันเพื่อเศรษฐกิจและความมั่นคงในความเป็นน้ำหนึ่งใจเดียวกัน");
		q.put("c2", "ประเทศสมาชิกรวมกันเพื่อมิตรภาพและความเป็นน้ำหนึ่งใจเดียวกัน");
		q.put("c3", "การรวมกลุ่มของประเทศสมาชิกร่วมกันประสานความเป็นน้ำหนึ่งและใจเดียวกัน");
		q.put("c4", "การรวมกลุ่มของประเทศสมาชิกเพื่อมิตรภาพและความเป็นน้ำหนึ่งในเดียวกัน");
		q.put("answer", "c2");
		questionSet.add(q);
		q.clear();*/
		
		q.put("question", "ข้อใดไม่ใช่หน้าที่ของชนชาวไทย");
		q.put("c1", "พิทักษ์รักษาไว้ซึ่งชาติ ศาสนา พระมหากษัตริย์ และประชาธิปไตย");
		q.put("c2", "ประชาชนทุกคนมีหน้าที่ในการดำเนินการตามกฎหมาย");
		q.put("c3", "รับราชการทหาร");
		q.put("c4", "ไปใช้สิทธิเลือกตั้ง");
		q.put("answer", "c2");
		questionSet.add(q);

		q = new HashMap<String,String>();
		/*q.put("question", "อาเซียนประกอบด้วยกี่ประเทศ อะไรบ้าง");
		q.put("c1", "9 ประเทศ คือ ไทย สิงคโปร์ มาเลเซีย อินโดนีเซีย ฟิลิปปินส์ ลาว พม่า กัมพูชา และบรูไน");
		q.put("c2", "10 ประเทศ ได้แก่ ไทย สิงคโปร์ มาเลเซีย อินโดนีเซีย ฟิลิปปินส์ ลาว พม่า กัมพูชา บรูไน และเวียดนาม");
		q.put("c3", "11 ประเทศ ได้แก่ ไทย สิงคโปร์ มาเลเซีย อินโดนีเซีย ฟิลิปปินส์ ลาว พม่า กัมพูชา บรูไน เวียดนาม และไต้หวัน");
		q.put("c4", "12 ประเทศ ได้แก่ ไทย สิงคโปร์ มาเลเซีย อินโดนีเซีย ฟิลิปปินส์ ลาว พม่า กัมพูชา บรูไน เวียดนาม ไต้หวัน และเกาหลีใต้");
		q.put("answer", "c2");
		questionSet.add(q);
		q.clear();*/
		
		q.put("question", "จำนวนคณะองคมนตรีกำหนดให้มีกี่คน");
		q.put("c1", "19 คน");
		q.put("c2", "18 คน");
		q.put("c3", "16 คน");
		q.put("c4", "12 คน");
		q.put("answer", "c1");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "อาเซียนหมายถึงอะไร");
		q.put("c1", "สมาคมประชาชาติแห่งเอเชียตะวันออกเฉียงใต้");
		q.put("c2", "สมาคมแห่งเอเชียตะวันออกเฉียงใต้เพื่อการพัฒนา");
		q.put("c3", "สมาคมเอเชียตะวันออกเฉียงใต้แห่งประชาชาติ");
		q.put("c4", "สมาคมประชาชาติเอเชียตะวันออกเฉียงใต้");
		q.put("answer", "c1");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "คำขวัญอาเซียน คือข้อใด");
		q.put("c1", "หนึ่งเดียว ประสานใจ รักษาอัตลักษณ์");
		q.put("c2", "หนึ่งวิสัยทัศน์ รักษาประชาธิปไตย ใส่ใจเศรษฐกิจร่วมกัน");
		q.put("c3", "ประชาคม ประชาธิปไตย ประชาชน");
		q.put("c4", "หนึ่งวิสัยทัศน์ หนึ่งอัตลักษณ์หนึ่งประชาคม");
		q.put("answer", "c4");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "รายได้ประชาชาติ วัดจากอะไร");
		q.put("c1", "จำนวนประชากร");
		q.put("c2", "อัตราการขยายตัวทางเศรษฐกิจของประเทศ");
		q.put("c3", "รายได้ของเกษตรกร");
		q.put("c4", "ถูกทุกข้อ");
		q.put("answer", "c4");
		questionSet.add(q);

		return questionSet; 
	}
	
}
