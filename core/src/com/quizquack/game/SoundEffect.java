package com.quizquack.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

public class SoundEffect {

	Music bgSound; 
	Sound correctSound, wrongSound;

	public SoundEffect() {
		bgSound = Gdx.audio.newMusic(Gdx.files.internal("sound/bgSound.mp3"));
		correctSound = Gdx.audio.newSound(Gdx.files.internal("sound/correct.mp3"));
		wrongSound = Gdx.audio.newSound(Gdx.files.internal("sound/wrong.mp3"));
		bgSound.setLooping(true);
		bgSound.setVolume(0.5f);
//		bgSound.setVolume(0.27f);
	}
	
	public void playBackgroundMusic() {
		bgSound.play();
	}
	
	public void playCorrectSound(){
		correctSound.play();
	}
	
	public void playWrongSound() {
		wrongSound.play();
	}
}
