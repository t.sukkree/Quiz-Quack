package com.quizquack.game;

import org.usb4java.Device;

public class McuWithPeriBoard extends McuBoard
{
	private static final byte RQ_SET_LED       = 9;
    private static final byte RQ_GET_SWITCH1    = 4;
    private static final byte RQ_GET_SWITCH2    = 3;
    private static final byte RQ_GET_SWITCH3    = 2;
    private static final byte RQ_GET_SWITCH4    = 1;
    
    private static final byte RQ_GET_SWITCH1B    = 8;
    private static final byte RQ_GET_SWITCH2B    = 7;
    private static final byte RQ_GET_SWITCH3B    = 6;
    private static final byte RQ_GET_SWITCH4B    = 5;

    public McuWithPeriBoard(Device device) {
		super(device);
	}

    /**
     * Set status of LED on peripheral board
     * 
     * @param ledNo  the number of LED (0,1,2) to set status
     * @param value  status to set (0-off, 1-on)
     */
    public void setLed(int ledNo, int value)
    {
        this.write(RQ_SET_LED, (short) ledNo, (short) value);
    }

    /**
     * Display a binary value on the peripheral board's LEDs
     * 
     * @param value The value to be displayed on the LEDs
     */
    /**
     * Check the state of the switch
     * 
     * @return true when the switch is pressed; false otherwise
     */
    public boolean getSwitch1()
    {
        byte[] ret = this.read(RQ_GET_SWITCH1, (short) 0, (short) 0);
        return ret[0] == 1;
    }
    
    public boolean getSwitch2()
    {
        byte[] ret = this.read(RQ_GET_SWITCH2, (short) 0, (short) 0);
        return ret[0] == 1;
    }
    
    public boolean getSwitch4()
    {
        byte[] ret = this.read(RQ_GET_SWITCH4, (short) 0, (short) 0);
        return ret[0] == 1;
    }
    
    public boolean getSwitch3()
    {
        byte[] ret = this.read(RQ_GET_SWITCH3, (short) 0, (short) 0);
        return ret[0] == 1;
    }
    //PORTB
    public boolean getSwitch1B()
    {
        byte[] ret = this.read(RQ_GET_SWITCH1B, (short) 0, (short) 0);
        return ret[0] == 1;
    }
    
    public boolean getSwitch2B()
    {
        byte[] ret = this.read(RQ_GET_SWITCH2B, (short) 0, (short) 0);
        return ret[0] == 1;
    }
    
    public boolean getSwitch4B()
    {
        byte[] ret = this.read(RQ_GET_SWITCH4B, (short) 0, (short) 0);
        return ret[0] == 1;
    }
    
    public boolean getSwitch3B()
    {
        byte[] ret = this.read(RQ_GET_SWITCH3B, (short) 0, (short) 0);
        return ret[0] == 1;
    }

    /**
     * Read and return the light intensity
     * 
     * @return a value between 0-1023; the greater the intensity, the higher the value
     */

}
