package com.quizquack.game;

import java.util.ArrayList;
import java.util.HashMap;

public class EntertainmentQuestion extends Question {
	public EntertainmentQuestion() {
		genQuestion();
		randomQuestion();
		randomChoice();
	}
	
	@Override
	public ArrayList getQuestionSet() {
		return questionSet;
	}
	
	public ArrayList<HashMap<String, String>> genQuestion() {
		questionSet = new ArrayList<HashMap<String,String>>();
		HashMap<String,String> q = new HashMap<String,String>();
		
		q.put("question", "ใครไม่เคยเป็นพิธีกรรายการเกมโชว์ พลิกล็อค");
		q.put("c1", "ปัญญา นิรันดร์กุล");
		q.put("c2", "ไตรภพ ลิมปพัทธ์");
		q.put("c3", "สัญญา คุณากร");
		q.put("c4", "เศรษฐา ศิระฉายา");
		q.put("answer", "c4");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "นักแสดงหญิงท่านใดไม่เคยแสดงภาพยนตร์ในตระกูล \"ฉลุย\"");
		q.put("c1", "เพ็ญ พิสุทธิ์");
		q.put("c2", "นัท มีเรีย");
		q.put("c3", "จันทร์จิรา จูแจ้ง");
		q.put("c4", "เมย์ พิชญ์นาฏ");
		q.put("answer", "c1");
		questionSet.add(q);
	//	System.out.println(questionSet.get(0));
		
		q = new HashMap<String,String>();
		q.put("question", "อาชีพใดที่พระเอกหนังร้อยล้าน \"เต๋อ ฉันทวิชช์\" เคยได้ทำ");
		q.put("c1", "ผู้เขียนบทภาพยนตร์");
		q.put("c2", "ผู้กำกับภาพยนตร์");
		q.put("c3", "นักแต่งเพลง");
		q.put("c4", "นักกีฬาเพาะกาย");
		q.put("answer", "c1");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "สมาชิกวงดนตรี \"คาราบาว\" ในปัจจุบัน มีทั้งหมดกี่คน");
		q.put("c1", "6");
		q.put("c2", "7");
		q.put("c3", "8");
		q.put("c4", "9");
		q.put("answer", "c3");
		questionSet.add(q);
		
		q = new HashMap<String,String>();
		q.put("question", "รายการโทรทัศน์รายการแรกของ \"เวิร์คพอยท์\" ที่ออกอากาศ คือรายการใด");
		q.put("c1", "ชิงร้อยชิงล้าน");
		q.put("c2", "เวทีทอง");
		q.put("c3", "คู่ทรหด");
		q.put("c4", "กามเทพผิดคิว");
		q.put("answer", "c2");
		questionSet.add(q);
		
		q = new HashMap<String,String>();
		q.put("question", "สมาชิก AF คนใดยังไม่ถ่ายแฟชั่นชุดว่ายน้ำขึ้นปกนิตยสาร");
		q.put("c1", "ซีแนม AF1");
		q.put("c2", "พัดชา AF2");
		q.put("c3", "มิ้น AF3");
		q.put("c4", "พะแพง AF4");
		q.put("answer", "c2");
		questionSet.add(q);
		
		q = new HashMap<String,String>();
		q.put("question", "นักร้องคนใดไม่เคยเป็นผู้ประกาศข่าวทางโทรทัศน์");
		q.put("c1", "สุนทร สุจริตฉันท์");
		q.put("c2", "ศันสนีย์ นาคพงศ์");
		q.put("c3", "สุทธิพงษ์ วัฒนจัง");
		q.put("c4", "สุทธิพงศ์ ทัดพิทักษ์กุล");
		q.put("answer", "c3");
		questionSet.add(q);
		
		q = new HashMap<String,String>();
		q.put("question", "นักร้องไทยคนใดที่ได้ถ่ายภาพเพื่อลงหนังสือภาพเรื่อง 7 วันในราชอาณาจักร");
		q.put("c1", "ธงไชย แมคอินไตย์");
		q.put("c2", "นันทิดา แก้วบัวสาย");
		q.put("c3", "ยืนยง โอภากุล");
		q.put("c4", "อัญชลี จงคดีกิจ");
		q.put("answer", "c4");
		questionSet.add(q);
		
		q = new HashMap<String,String>();
		q.put("question", "ตัวการ์ตูน โพโคจัง ในโฆษณาผ้าอ้อมเด็กมามี่โพโค มีที่มาจากสัตว์ตัวใด");
		q.put("c1", "หมี");
		q.put("c2", "แมว");
		q.put("c3", "นกเพนกวิน");
		q.put("c4", "มนุษย์");
		q.put("answer", "c1");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "\"บุญธรรม ฮวดกระโทก\" คือชื่อจริงของใคร");
		q.put("c1", "ศรีหนุ่ม เชิญยิ้ม");
		q.put("c2", "นุ้ย เชิญยิ้ม");
		q.put("c3", "หม่ำ จ๊กมก");
		q.put("c4", "บุญธรรม พระประโทน");
		q.put("answer", "c1");
		questionSet.add(q);
		
		q = new HashMap<String,String>();
		q.put("question", "\"คริส หอวัง\" มีน้องสาวชื่อว่าอะไร");
		q.put("c1", "นุ่น");
		q.put("c2", "พลอย");
		q.put("c3", "จอย");
		q.put("c4", "พุธ");
		q.put("answer", "c2");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "นางสาวไทยคนแรกที่เป็นด็อกเตอร์คือใคร");
		q.put("c1", "อารียา สิริโสภา");
		q.put("c2", "สุจิรา อรุณพิพัฒน์");
		q.put("c3", "ลลนา ก้องธรนินทร์");
		q.put("c4", "ปนัดดา วงศ์ผู้ดี");
		q.put("answer", "c4");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "ละครหลังข่าวภาคค่ำเรื่องแรกที่ \"เจเอสแอล\" ทำให้กับช่อง 7 คือเรื่องใด");
		q.put("c1", "เงา");
		q.put("c2", "ราชินีลูกทุ่ง พุ่มพวง ดวงจันทร์");
		q.put("c3", "บริษัทจัดคู่");
		q.put("c4", "เทวดาตกสวรรค์");
		q.put("answer", "c3");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "\"สมใจ\" กับ \"สุขใจ\" คือชื่อลูกของดาราท่านใด");
		q.put("c1", "มอส ปฏิภาณ");
		q.put("c2", "เต๋า สมชาย");
		q.put("c3", "ปอ ทฤษฎี");
		q.put("c4", "เด๋อ ดอกสะเดา");
		q.put("answer", "c2");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "\"ผมทำเพื่อความมันส์ครับพี่\" เป็นชื่ออัลบั้มเพลงชุดแรกของนักแสดงท่านใด");
		q.put("c1", "ไพโรจน์ ใจสิงห์");
		q.put("c2", "ไพโรจน์ สังวริบุตร");
		q.put("c3", "ทูน หิรัญทรัพย์");
		q.put("c4", "สรพงศ์ ชาตรี");
		q.put("answer", "c2");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "ผลงานการแสดงภาพยนตร์เรื่องแรกของ \"แอ๊ด คาราบาว\" คือเรื่องใด");
		q.put("c1", "เสียงเพลงแห่งเสรีภาพ");
		q.put("c2", "คนดีที่บ้านด่าน");
		q.put("c3", "บุญชู ผู้น่ารัก");
		q.put("c4", "สวรรค์บ้านนา");
		q.put("answer", "c4");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "โลโก้ \"ช่อง 7 สี\" มีทั้งหมดกี่สี");
		q.put("c1", "6");
		q.put("c2", "7");
		q.put("c3", "8");
		q.put("c4", "9");
		q.put("answer", "c3");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "รายการใดของช่อง 8 ที่ไม่มีช่วงแนะนำสินค้าท้ายรายการ");
		q.put("c1", "คุยเพลินเมืองไทย");
		q.put("c2", "ปากโป้ง");
		q.put("c3", "เอิ๊กโชว์");
		q.put("c4", "ดิ อินฟินิตี้ เกมไม่รู้จบ");
		q.put("answer", "c4");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "ซูโม่คนใดในรายการเพชฌฆาตความเครียด ที่ไม่ได้เป็นนิสิตคณะ 'ถาปัด จุฬาฯ");
		q.put("c1", "ซูโม่ตู้");
		q.put("c2", "ซูโม่โค้ก");
		q.put("c3", "ซูโม่เป๊ปซี่");
		q.put("c4", "ซูโม่กิ๊ก");
		q.put("answer", "c3");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "ดาราคนใดที่เคยเข้าประกวด \"ดัชชี่บอย\"");
		q.put("c1", "บี้ สุกฤษฎ์");
		q.put("c2", "อ๊อฟ ปองศักดิ์");
		q.put("c3", "อั้ม อธิชาติ");
		q.put("c4", "ณัฐ ทิวไผ่งาม");
		q.put("answer", "c3");
		questionSet.add(q);

		return questionSet; 
	
	}
}
