package com.quizquack.game;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Question {
	
	ArrayList<HashMap<String,String>> questionSet;
	Random r;
	int[] selectedQuestion; 
	String[] rearrangedChoice;
			
	public Question() {
		r = new Random();
	}

	public HashMap<String,String> getQuestion(int i) {
		return questionSet.get(selectedQuestion[i]);
	}
	
	public String[] getRandomChoice() {
		return rearrangedChoice;
	}
	
	public void randomQuestion() {
		selectedQuestion = new int[5];
		int count = 0;
		questionSet = this.getQuestionSet();
		while(count < 5)
		{		
			int number = r.nextInt(questionSet.size());
			for(int i = 0; i < 5; i++){
				if (number == selectedQuestion[i]) {
					number = r.nextInt(questionSet.size());
					i = -1;
				}
			}
			selectedQuestion[count++] = number;
		}
	}
	
	public void randomChoice() {
		rearrangedChoice = new String[4];
		int count = 0;
		while(count < 4){
			int number = r.nextInt(4)+1;
			for(int i = 0; i < 4; i++){
				String keep = "c"+number;
				if (keep.equals(rearrangedChoice[i])) {
					number = r.nextInt(4)+1;
					i = -1;
				}
			}
			rearrangedChoice[count++] = "c"+number;
		}
	}
	
	public ArrayList<HashMap<String,String>> getQuestionSet() {
		ArrayList<HashMap<String,String>> a = new ArrayList();
		HashMap<String,String> q = new HashMap<String,String>();
		q.put("a", "a");
		questionSet.add(q);
		return a;
	}
	
}
