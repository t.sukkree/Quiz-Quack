package com.quizquack.game;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

public class WorldRenderer {
	
	private QuizQuack quizQuack;
	private SpriteBatch batch;
	private FreeTypeFontGenerator generator; 	
	private FreeTypeFontParameter parameter;
	private BitmapFont choiceFont,questionFont;
	private Texture trophy, background, egg, soso;
	private String characters = "1234567890qwer=tyuiopas!-dfghjkl;'zxcvbnm,./QWERTYUIOP{}ASDF+-*/GHJKL:ZXCVBNM<>?-๑๒๓๔ู฿๕๖๗๘๙ๆไำพะัีรนยบลฃฟหกดเ้่าสวงผปแอิืทมใฝ๐ฎฑธํ๊ณฯญฐ,ฅฤฆฏโฌ็๋ษศซ.()ฉฮฺ์?ฒฬฦๅ/-ภถุึคตจขช"; 
	private HashMap<String,String> question;
	private World world;
	private String q,c1,c2,c3,c4;
	private String[] choice;
	private int player1_score = 0, player2_score = 0, p1status = 0, p2status = 0;
	private int timer,winner,round;
	private int[] winp1 = new int[3],winp2 = new int[3];
	private Texture[] winstatus,winstatus2;
	
	public WorldRenderer(QuizQuack quizquack, World world) {
		 this.quizQuack = quizquack;
		 this.world = world;
		 batch = quizQuack.batch;
		 genFont();
		 background = new Texture("daffy-duck.jpg");
		 winstatus = new Texture[3];
		 winstatus[2] = new Texture("trophy.png");
		 winstatus[0] = new Texture("RottenEgg.png");
		 winstatus[1] = new Texture("soso.png");
		 winstatus2 = new Texture[3];
		 winstatus2[2] = new Texture("trophy2.png");
		 winstatus2[0] = new Texture("RottenEgg2.png");
		 winstatus2[1] = new Texture("soso2.png");
	}
	
	public void update(float delta){
		render(delta);
		question = world.getQuestion();
		choice = world.getChoice();
		if(quizQuack.GAME_STATE == quizQuack.PLAY_MODE){
			setQuestion();
		}
		player1_score = world.getScorep1();
		player2_score = world.getScorep2();
		timer = world.getTimer();
		winner = world.getWinner();
		p1status = world.getP1Status();
		p2status = world.getP2Status();
		winp1 = world.getWinp1();
		winp2 = world.getWinp2();
		round = world.getRound();
	}
	
	private void setQuestion(){
		q = question.get("question");
		c1 = question.get(choice[0]);
		c2 = question.get(choice[1]);
		c3 = question.get(choice[2]);
		c4 = question.get(choice[3]);
	}
	
	public void genFont() {
		generator = new FreeTypeFontGenerator(Gdx.files.internal("font/BoonJot-400.ttf")); 	     
		parameter = new FreeTypeFontParameter(); 	     
		parameter.size = 30; 	     
		parameter.color = Color.WHITE; 	     
		parameter.characters = characters;
		choiceFont = generator.generateFont(parameter);
		parameter.size = 35; 
		parameter.color = Color.YELLOW;
		questionFont = generator.generateFont(parameter);
	}
	
	public void render (float delta) {
		batch.begin();
		if(quizQuack.GAME_STATE == quizQuack.PLAY_MODE) {
			questionFont.draw(batch,""+q,40,500);
			choiceFont.draw(batch,"1. "+c1,40,400);
			choiceFont.draw(batch,"2. "+c2,40,300);
			choiceFont.draw(batch,"3. "+c3,40,200);
			choiceFont.draw(batch,"4. "+c4,40,100);
			choiceFont.draw(batch,""+player1_score,20,650);
			choiceFont.draw(batch,""+player2_score,1250,650);
			choiceFont.draw(batch, "round "+round, 570, 710);
			choiceFont.draw(batch,""+timer, 600,650);
			for(int i = 0; i < 3; i++){
				if(winp1[i] > 0){
				batch.draw(winstatus[winp1[i]-1],30+(i*50),700);
				} if(winp2[i] > 0){
				batch.draw(winstatus[winp2[i]-1],1100+(i*50),700);
				}
			}
		} else if(quizQuack.GAME_STATE == quizQuack.CHOOSE_MODE) {
			choiceFont.draw(batch, "round "+round, 570, 710);
			questionFont.draw(batch, "select mode by player "+world.getPlayerSelected(),450,600);
			questionFont.draw(batch, "ความรู้ทั่วไป",550,450);
			questionFont.draw(batch, "บันเทิง",550,350);
			questionFont.draw(batch, "วิทยาศาสตร์",550,150);
			questionFont.draw(batch, "คณิตศาสตร์",550,250);
			for(int i = 0; i < 3; i++){
				if(winp1[i] > 0){
				batch.draw(winstatus2[winp1[i]-1],30+(i*100),500);
				} if(winp2[i] > 0){
				batch.draw(winstatus2[winp2[i]-1],950+(i*100),500);
				}
			}
		} else if(quizQuack.GAME_STATE == quizQuack.SHOW_RESULT) {
			batch.draw(winstatus2[2], 610, 550);
			if(winner != 0){
				questionFont.draw(batch, "PLAYER " + winner + " WIN",580,450);
			} else{
				questionFont.draw(batch, "A TIE!",620,450);
			}
			questionFont.draw(batch, "press button 1 to start game" , 450, 350);
			if(p1status == 1){
				questionFont.draw(batch, "player 1 is ready" , 100, 250);
				questionFont.draw(batch, "waiting..." , 1000, 250);
			}
			else if(p2status == 1){
				questionFont.draw(batch, "player 2 is ready" , 1000, 250);
				questionFont.draw(batch, "waiting..." , 100, 250);
			}
		} else if(quizQuack.GAME_STATE == quizQuack.START_GAME) {
			batch.draw(background, 0, -10);
			questionFont.draw(batch, "press button 1 to start game" , 450, 70);
			if(p1status == 1){
				questionFont.draw(batch, "player 1 is ready" , 100, 400);
				questionFont.draw(batch, "waiting..." , 1000, 400);
			}
			else if(p2status == 1){
				questionFont.draw(batch, "player 2 is ready" , 1000, 400);
				questionFont.draw(batch, "waiting..." , 100, 400);
			}
		} 
		batch.end();
	}
}
