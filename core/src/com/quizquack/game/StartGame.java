package com.quizquack.game;

public class StartGame {
	
	private int p1_status = 0, p2_status = 0;
	private int goFirst = 0;
	private int count = 0;
	
	private QuizQuack quizQuack;
	private World world;
	
	public StartGame(QuizQuack quizQuack,World world){
		this.quizQuack = quizQuack;
		this.world = world;
	}
	
	public void ready(int player){
		if(count == 0){
			goFirst = player;
		}
		if(player == 1 && p1_status == 0){
			p1_status = 1;
			world.setStatus(1);
			count++;
		} else if (player == 2 && p2_status == 0){
			p2_status = 1;
			world.setStatus(2);
			count++;
		}
		if(count == 2 && p1_status == 1 && p2_status == 1){
			world.reset();
			world.choosemodeP = goFirst;
			quizQuack.GAME_STATE = quizQuack.CHOOSE_MODE;
			count = 0;
			p1_status = 0;
			p2_status= 0;
			world.clearp1 = true;
			world.clearp2 = true;
		}
	}
	
}
