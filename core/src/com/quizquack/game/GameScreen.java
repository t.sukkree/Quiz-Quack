package com.quizquack.game;

import org.usb4java.Device;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;

public class GameScreen extends ScreenAdapter {

	private QuizQuack quizQuack;
	WorldRenderer worldRenderer;
	World world;
	StartGame startGame;
	private McuWithPeriBoard peri;
	
	private float counttimesw = 0;
	
	private boolean swp11 = false;
	private boolean swp12 = false;
	private boolean swp13 = false;
	private boolean swp14 = false;
	private boolean swp21 = false;
	private boolean swp22 = false;
	private boolean swp23 = false;
	private boolean swp24 = false;
	
	
	public GameScreen(QuizQuack quizQuack) {
		McuBoard.initUsb();
		this.quizQuack = quizQuack;
		world = new World(quizQuack,this);
		worldRenderer = new WorldRenderer(quizQuack,world);
		startGame = world.getStartGame();
		initMCU();
		closeLight();
		//McuBoard.cleanupUsb();
	}
	
	@Override 	
	public void render(float delta) { 	
		Gdx.gl.glClearColor(0, 0, 0, 1); 	    
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); 	
		world.update(delta);
		worldRenderer.update(delta);
		counttimesw += delta;
		if(counttimesw>0.05 && counttimesw<0.09){
			updateMCU();
			counttimesw = 0;
		}
		update(delta);
	} 	 	
	
	private void initMCU(){
		Device[] devices = McuBoard.findBoards();
    	if (devices.length == 0) {
            System.out.format("** Practicum board not found **\n");
            return;
    	}
    	else {
            System.out.format("** Found %d practicum board(s) **\n", devices.length);
    	}
    	peri = new McuWithPeriBoard(devices[0]);
    	 System.out.println("** board **\n");
 
	}
	
	private void updateMCU(){
		 if(world.swClear){
    		if(peri.getSwitch1() && world.clearp2){
    			swp21 = true;
    		} if(peri.getSwitch2() && world.clearp2){
    			swp22 = true;
    		} if(peri.getSwitch3() && world.clearp2){
    			swp23 = true;
    		} if(peri.getSwitch4() && world.clearp2){
    			swp24 = true;
    		} 
    		if(peri.getSwitch1B() && world.clearp1){
    			swp11 = true;
    		} if(peri.getSwitch2B() && world.clearp1){
    			swp12 = true;
    		} if(peri.getSwitch3B() && world.clearp1){
    			swp13 = true;
    		} if(peri.getSwitch4B() && world.clearp1){
    			swp14 = true;
    		}
		 }
	}
	
	public void showLight(int player,int point){
		if(point == 1){
			if(player == 1){
				peri.setLed(6, 1);
			} else {
				peri.setLed(0, 1);
			}
		} else{
			if(player == 1){
				peri.setLed(5, 1);
			} else {
				peri.setLed(1, 1);
			}
		}
	}
	
	public void clearSW(){
		if(!peri.getSwitch1() && !peri.getSwitch2() && !peri.getSwitch3() && !peri.getSwitch4()){
			world.clearp2 = true;
		} if(!peri.getSwitch1B() && !peri.getSwitch2B() && !peri.getSwitch3B() && !peri.getSwitch4B()){
			world.clearp1 = true;
		}
		if(world.clearp1 && world.clearp2){
			world.swClear = true;
		}
	}
	
	public void closeLight(){
			peri.setLed(1, 0);
			peri.setLed(0, 0);
			peri.setLed(5, 0);
			peri.setLed(6, 0);	
	}
	
	private void update(float delta) {
		
		if(!world.swClear){
			clearSW();
		}
		if(world.swClear){
		if(quizQuack.GAME_STATE == quizQuack.PLAY_MODE) {
			if (Gdx.input.isKeyJustPressed(Keys.NUM_1) || swp11) { 				
				world.clearp1 = false;
				world.answer(0,1);
				swp11 = false;
			} else if (Gdx.input.isKeyJustPressed(Keys.NUM_2) || swp12) { 				
				world.clearp1 = false;
				world.answer(1,1);
				swp12 = false;
			} else if (Gdx.input.isKeyJustPressed(Keys.NUM_3) || swp13) { 				
				world.clearp1 = false;
				world.answer(2,1);
				swp13 = false;
			} else if (Gdx.input.isKeyJustPressed(Keys.NUM_4) || swp14) { 				
				world.clearp1 = false;
				world.answer(3,1);
				swp14 = false;
			} 
			
			if (Gdx.input.isKeyJustPressed(Keys.NUM_5) || swp21) { 				
				world.clearp2 = false;
				world.answer(0,2);
				swp21 = false;
			} else if (Gdx.input.isKeyJustPressed(Keys.NUM_6) || swp22) { 				
				world.clearp2 = false;
				world.answer(1,2);
				swp22 = false;
			} else if (Gdx.input.isKeyJustPressed(Keys.NUM_7) || swp23) { 				
				world.clearp2 = false;
				world.answer(2,2);
				swp23 = false;
			} else if (Gdx.input.isKeyJustPressed(Keys.NUM_8) || swp24) { 				
				world.clearp2 = false;
				world.answer(3,2);
				swp24 = false;
			}
		} else if(quizQuack.GAME_STATE == quizQuack.CHOOSE_MODE) {
			if (Gdx.input.isKeyJustPressed(Keys.NUM_1) || swp11) { 				
				world.clearp1 = false;
				world.chooseMode(0,1);
				swp11 = false;
			} else if (Gdx.input.isKeyJustPressed(Keys.NUM_2) || swp12) { 				
				world.clearp1 = false;
				world.chooseMode(1,1);
				swp12 = false;
			} else if (Gdx.input.isKeyJustPressed(Keys.NUM_3) || swp13) { 				
				world.clearp1 = false;
				world.chooseMode(2,1);
				swp13 = false;
			} else if (Gdx.input.isKeyJustPressed(Keys.NUM_4) || swp14) { 				
				world.clearp1 = false;
				world.chooseMode(3,1);
				swp14 = false;
			} else if (Gdx.input.isKeyJustPressed(Keys.NUM_5) || swp21) { 				
				world.clearp2 = false;
				world.chooseMode(0,0);				
				swp21 = false;
			} else if (Gdx.input.isKeyJustPressed(Keys.NUM_6) || swp22) { 				
				world.clearp2 = false;
				world.chooseMode(1,0);
				swp22 = false;
			} else if (Gdx.input.isKeyJustPressed(Keys.NUM_7) || swp23) { 				
				world.clearp2 = false;
				world.chooseMode(2,0);
				swp23 = false;
			} else if (Gdx.input.isKeyJustPressed(Keys.NUM_8) || swp24) { 				
				world.clearp2 = false;
				world.chooseMode(3,0);
				swp24 = false;
			}
		} else if((quizQuack.GAME_STATE == quizQuack.START_GAME || quizQuack.GAME_STATE == quizQuack.SHOW_RESULT)) {
			if (Gdx.input.isKeyJustPressed(Keys.NUM_1) || swp11) { 	
				System.out.println("ter");
				startGame.ready(1);
				world.clearp1 = false;
				swp11 = false;
			} else if (Gdx.input.isKeyJustPressed(Keys.NUM_5) || swp21) { 				
				startGame.ready(2);
				world.clearp2 = false;
				swp21 = false;
			}
		}
		}
	}
}