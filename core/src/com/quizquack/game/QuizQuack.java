package com.quizquack.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class QuizQuack extends Game {
	
	public SpriteBatch batch;
	public int GAME_STATE = 4;
	public static final int CHOOSE_MODE = 1;
	public static final int PLAY_MODE = 2;
	public static final int SHOW_RESULT = 3;
	public static final int START_GAME = 4;
	
	public int TYPE = -1;
	public static final int GEN_KNOWLEDGE = 0;
	public static final int ENTERTAINMENT = 1;
	public static final int MATH = 2;
	public static final int SCIENCEQ = 3;

	@Override
	public void create () {
		batch = new SpriteBatch();
		setScreen(new GameScreen(this));
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
	}
}
