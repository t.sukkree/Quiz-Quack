package com.quizquack.game;

import java.util.ArrayList;
import java.util.HashMap;

public class MathQuestion extends Question {
	public MathQuestion() {
		genQuestion();
		randomQuestion();
		randomChoice();
	}
	
	@Override
	public ArrayList getQuestionSet() {
		return questionSet;
	}
	
	public ArrayList<HashMap<String, String>> genQuestion() {
		questionSet = new ArrayList<HashMap<String,String>>();
		HashMap<String,String> q = new HashMap<String,String>();
		
		q.put("question", "33-6=");
		q.put("c1", "27");
		q.put("c2", "29");
		q.put("c3", "30");
		q.put("c4", "31");
		q.put("answer", "c1");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "100+561=");
		q.put("c1", "661");
		q.put("c2", "561");
		q.put("c3", "461");
		q.put("c4", "761");
		q.put("answer", "c1");
		questionSet.add(q);
	//	System.out.println(questionSet.get(0));
		
		q = new HashMap<String,String>();
		q.put("question", "1000-520=");
		q.put("c1", "480");
		q.put("c2", "580");
		q.put("c3", "440");
		q.put("c4", "540");
		q.put("answer", "c1");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "5x5x6=");
		q.put("c1", "170");
		q.put("c2", "160");
		q.put("c3", "150");
		q.put("c4", "140");
		q.put("answer", "c3");
		questionSet.add(q);
		
		q = new HashMap<String,String>();
		q.put("question", "6x12=");
		q.put("c1", "80");
		q.put("c2", "74");
		q.put("c3", "72");
		q.put("c4", "76");
		q.put("answer", "c3");
		questionSet.add(q);
		
		q = new HashMap<String,String>();
		q.put("question", "6x7=");
		q.put("c1", "42");
		q.put("c2", "36");
		q.put("c3", "38");
		q.put("c4", "34");
		q.put("answer", "c1");
		questionSet.add(q);
		
		q = new HashMap<String,String>();
		q.put("question", "5x9=");
		q.put("c1", "30");
		q.put("c2", "35");
		q.put("c3", "45");
		q.put("c4", "55");
		q.put("answer", "c3");
		questionSet.add(q);
		
		q = new HashMap<String,String>();
		q.put("question", "8x8=");
		q.put("c1", "52");
		q.put("c2", "64");
		q.put("c3", "73");
		q.put("c4", "56");
		q.put("answer", "c2");
		questionSet.add(q);
		
		q = new HashMap<String,String>();
		q.put("question", "3x3=");
		q.put("c1", "6");
		q.put("c2", "9");
		q.put("c3", "12");
		q.put("c4", "15");
		q.put("answer", "c2");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "8x5=");
		q.put("c1", "35");
		q.put("c2", "40");
		q.put("c3", "30");
		q.put("c4", "45");
		q.put("answer", "c2");
		questionSet.add(q);
		
		q = new HashMap<String,String>();
		q.put("question", "(4+5)+5");
		q.put("c1", "8");
		q.put("c2", "9");
		q.put("c3", "7");
		q.put("c4", "14");
		q.put("answer", "c4");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "(7-5)+2=");
		q.put("c1", "5");
		q.put("c2", "3");
		q.put("c3", "4");
		q.put("c4", "6");
		q.put("answer", "c3");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "7x5=");
		q.put("c1", "56");
		q.put("c2", "89");
		q.put("c3", "35");
		q.put("c4", "34");
		q.put("answer", "c2");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "(9-4)+5");
		q.put("c1", "6");
		q.put("c2", "4");
		q.put("c3", "12");
		q.put("c4", "10");
		q.put("answer", "c4");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "38-12=");
		q.put("c1", "32");
		q.put("c2", "26");
		q.put("c3", "24");
		q.put("c4", "30");
		q.put("answer", "c2");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "54+98=");
		q.put("c1", "152");
		q.put("c2", "162");
		q.put("c3", "142");
		q.put("c4", "132");
		q.put("answer", "c1");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "99x2=");
		q.put("c1", "192");
		q.put("c2", "198");
		q.put("c3", "196");
		q.put("c4", "194");
		q.put("answer", "c2");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "959+123=");
		q.put("c1", "1082");
		q.put("c2", "1072");
		q.put("c3", "1062");
		q.put("c4", "1052");
		q.put("answer", "c1");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "990/3");
		q.put("c1", "220");
		q.put("c2", "330");
		q.put("c3", "440");
		q.put("c4", "550");
		q.put("answer", "c2");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "ข้อใดมีผลลัพธ์เท่ากับ 88");
		q.put("c1", "25+63");
		q.put("c2", "73+21");
		q.put("c3", "64+32");
		q.put("c4", "22+44");
		q.put("answer", "c1");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "ผลลัพธ์ในข้อใดมีค่ามากที่สุด");
		q.put("c1", "75-22");
		q.put("c2", "98-51");
		q.put("c3", "86-32");
		q.put("c4", "100-87");
		q.put("answer", "c3");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "ข้อใดถูกต้อง");
		q.put("c1", "67-24 = 53");
		q.put("c2", "53+21 = 74");
		q.put("c3", "92-80 = 22");
		q.put("c4", "41+22 = 64");
		q.put("answer", "c2");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "ข้อใดมีผลลัพธ์ต่างจาก 85 อยู่ 3");
		q.put("c1", "35+53");
		q.put("c2", "42+42");
		q.put("c3", "51+15");
		q.put("c4", "57+22");
		q.put("answer", "c1");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "83 - 21 กับ 32 + 13 ผลลัพธ์มีค่าต่างกันเท่าไร");
		q.put("c1", "16");
		q.put("c2", "17");
		q.put("c3", "18");
		q.put("c4", "19");
		q.put("answer", "c2");
		questionSet.add(q);

		q = new HashMap<String,String>();
		q.put("question", "\"กบสอบวิชาคณิตศาสตร์ได้ 48 คะแนน วิทยาศาสตร์ได้ 42 คะแนน\" ข้อใดถูกต้อง");
		q.put("c1", "กบสอบวิชาคณิตศาสตร์ได้คะแนนน้อยกว่าวิทยาศาสตร์ 5 คะแนน");
		q.put("c2", "กบสอบวิชาวิทยาศาสตร์ได้คะแนนน้อยกว่าคณิตศาสตร์ 4 คะแนน");
		q.put("c3", "กบสอบวิชาคณิตศาสตร์ได้คะแนนมากกว่าวิทยาศาสตร์ 6 คะแนน");
		q.put("c4", "กบสอบวิชาวิทยาศาสตร์ได้คะแนนมากกว่าวิชาคณิตศาสตร์ 6 คะแนน");
		q.put("answer", "c3");
		questionSet.add(q);

		return questionSet; 
	
	}
}
