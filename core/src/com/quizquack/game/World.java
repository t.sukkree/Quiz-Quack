package com.quizquack.game;

import java.util.HashMap;

public class World {
		private GenKnowledge genKnowledge;
		private MathQuestion mathQuestion;
		private ScienceQuestion scienceQuestion;
		private EntertainmentQuestion entertainment;
		private GameScreen gameScreen;
		private SoundEffect sound;
		HashMap<String,String> question;
		String[] choice;
		private StartGame startGame;
		
		private int scorep1 = 0, scorep2 = 0;
		private int[] winp1 = new int [3], winp2 = new int [3];
		private int numQues=0;
		private boolean checkP1 = false;
		private boolean checkP2 = false;
		private float time = 21;
		private QuizQuack quizQuack;
		private Question[] qMode;
		private int modeSelect = 0;
		int choosemodeP = 1;
		private int round = 0,winner = 0;
		private int p1status=0,p2status=0;
		private float countChangeQuestion = 0;
		private boolean checkChangeQ = false;
		
		public boolean clearp1 = true;
		public boolean clearp2 = true;
		
		public boolean swClear = true;
		
		public World(QuizQuack quizQuack,GameScreen gameScreen) {
			this.quizQuack = quizQuack;
			this.gameScreen = gameScreen;
			sound = new SoundEffect();
			startGame = new StartGame(quizQuack,this);
			qMode = new Question[4];
			//genKnowledge = new GenKnowledge();
			qMode[0] = new GenKnowledge();
			qMode[2] = new MathQuestion();
			qMode[3] = new ScienceQuestion();
			qMode[1] = new EntertainmentQuestion();
			sound.playBackgroundMusic();
		}
		
		public void update(float delta) {
			if(quizQuack.GAME_STATE == quizQuack.PLAY_MODE){
				showQuestion(delta);
				if(checkChangeQ){
					countChangeQuestion += delta;
					System.out.println(countChangeQuestion);
					if((int)countChangeQuestion == 2){
						gameScreen.closeLight();
						numQues++;
						time = 21;
						checkP1 = false;
						checkP2 = false;
						swClear = false;
						checkChangeQ = false;
					}
				}
			}
		}
		
		public void chooseMode(int mode,int player){
			if(player == (choosemodeP%2)){
				modeSelect = mode;
				System.out.println("                                      choosemodeP");
				System.out.println("                                     "+clearp1);
				quizQuack.GAME_STATE = quizQuack.PLAY_MODE;
				swClear = false;
				choosemodeP++;
			//	clearp1 = true;
			//	clearp2 = true;
			}
			
		}
		
		public void beforeNextRound(){
			numQues = 0;
			if(scorep1 > scorep2){
				winp1[round] = 3;
				winp2[round] = 1;
			} else if(scorep2 > scorep1){
				winp2[round] = 3;
				winp1[round] = 1;
			}else{
				winp2[round] = 2;
				winp1[round] = 2;
			}
			round ++;
			scorep1 = 0;
			scorep2 = 0;
			if(round == 3){
				int count1 = 0, count2 = 0;
				for(int i = 0; i < 3; i++){
					if(winp1[i] == 3){
						count1++;
					} else if (winp2[i] == 3){
						count2++;
					}
				}
				if(count1>count2){
					winner = 1;
				}else if(count2>count1){
					winner = 2;
				} 
				quizQuack.GAME_STATE = quizQuack.SHOW_RESULT;
			}
		}
		
		public int getWinner(){
			return winner;
		}
		
		public int getPlayerSelected(){
			if(choosemodeP%2 == 0)
				return 2;
			return 1; 
		}
		public void showQuestion(float delta) {
			if(numQues < 5 && round <3){
				//question = genKnowledge.getQuestion(numQues);
				//choice = genKnowledge.getRandomChoice();
				question = qMode[modeSelect].getQuestion(numQues);
				choice = qMode[modeSelect].getRandomChoice();
				time -= delta;
				if((int)time == 0){
					numQues++;
					time = 20;
				}
			} else if(numQues == 5) {
				beforeNextRound();
				if(round <= 2)
					quizQuack.GAME_STATE = quizQuack.CHOOSE_MODE;
			}
		}
		
		public int getScorep1(){
			return scorep1;
		}
		
		public int getScorep2(){
			return scorep2;
		}
		
		public int getTimer(){
			return (int)time;
		}
		
		public int getP1Status(){
			return p1status;
		}
		
		public int getP2Status(){
			return p2status;
		}
		
		public int[] getWinp1(){
			return winp1;
		}
		
		public int[] getWinp2(){
			return winp2;
		}
		
		public int getRound(){
			return round+1;
		}
		
		public void setStatus(int i){
			if(i == 1)
				p1status =1;
			else if(i == 2)
				p2status =1;
		}
		
		public StartGame getStartGame(){
			return startGame;
		}
		
		public void answer(int i,int player){
			String ans = question.get("answer");
			if(ans.equals(choice[i])) {
				if(player == 1 && !checkP1) {
					scorep1 += (int)time;
					checkP1 = true;
					gameScreen.showLight(1,1);
					
				}
				else if(player == 2 && !checkP2) {
					scorep2 += (int)time;
					checkP2 = true;
					gameScreen.showLight(2,1);
				}
				sound.playCorrectSound();
			}
			else{
				if(player == 1 && !checkP1) {
					checkP1 = true;
					gameScreen.showLight(1,0);
				} else if (player == 2 && !checkP2){
					checkP2 = true;
					gameScreen.showLight(2,0);
				}
				sound.playWrongSound();
			}
			if(checkP1 && checkP2) {
				countChangeQuestion = 0;
				checkChangeQ = true;
				//clearp1 = true;
				//clearp2 = true;
				//gameScreen.closeLight();
			}
		}
		
		public void reset(){
			qMode[0] = new GenKnowledge();
			qMode[2] = new MathQuestion();
			qMode[3] = new ScienceQuestion();
			qMode[1] = new EntertainmentQuestion();
			gameScreen.closeLight();
			scorep1 = 0; scorep2 = 0;
			winp1 = new int [3];
			winp2 = new int [3];
			numQues=0;
			checkP1 = false;
			checkP2 = false;
			time = 21;
			modeSelect = 0;
			choosemodeP = 1;
			round = 0;
			winner = 0;
			p1status = 0;
			p2status = 0;
		}
		
		public HashMap<String,String> getQuestion(){
			return question;
		}
	
		public String[] getChoice(){
			return choice;
		}
}
